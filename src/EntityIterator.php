<?php

namespace Drupal\entity_iterator;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a memory efficient way to loop through entities.
 */
class EntityIterator implements \Iterator, \ArrayAccess, \Countable {

  /**
   * The entity storage for the supplied entity type.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The size of chunks to create.
   *
   * @var int
   */
  protected $chunkSize;

  /**
   * The ids to iterate over.
   *
   * @var array
   */
  protected $ids;

  /**
   * The entity ids, pre-chunked.
   *
   * @var array[]
   */
  protected $idChunks;

  /**
   * The current entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $currentEntity;

  /**
   * The current set of loaded entities.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $loadedEntities;

  /**
   * The static memory cache that will need clearing.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $memoryCache;

  /**
   * Create a new instance of the entity iterator.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $ids
   *   The ids to load, NULL to load all.
   * @param int $chunk_size
   *   The chunk size, defaults to 50.
   * @param string $bundle
   *   The bundle machine name to iterate over.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(string $entity_type_id, array $ids = NULL, $chunk_size = 50, string $bundle = NULL) {
    $this->entityStorage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    $this->memoryCache = \Drupal::service('entity.memory_cache');

    // Initial part of the query.
    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $query = $this->entityStorage->getQuery()->accessCheck(FALSE);
    if ($bundle && $entity_type->getBundleEntityType() != NULL) {
      $bundle_key = $entity_type->getKey('bundle');
      $query->condition($bundle_key, $bundle);
    }

    // Fetch the ids if null passed in.
    if ($ids === NULL) {
      $ids = $query->execute();
    }
    elseif (!empty($ids)) {
      $ids = $query->condition($entity_type->getKey('id'), $ids, 'IN')->execute();
    }

    // Store the ids to iterate over, we can't chunk them because
    // rewind() will need to reset the chunks.
    $this->ids = $ids;
    $this->chunkSize = $chunk_size;

    // Initialise.
    $this->loadedEntities = [];
    $this->currentEntity = NULL;
    $this->idChunks = [];
  }

  /**
   * Destructor.
   */
  public function __destruct() {
    $this->cleanUp();
  }

  /**
   * Function to clean up any loaded entities.
   */
  protected function cleanUp(): void {
    // Clear any current entity.
    if ($this->currentEntity) {
      $this->removeEntityFromMemoryCache($this->currentEntity);
    }

    // Clear any existing loaded entities from the static cache.
    foreach ($this->loadedEntities as $entity) {
      $this->removeEntityFromMemoryCache($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function current(): mixed {
    return $this->currentEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function next(): void {
    // Clean static cache of entity we're leaving behind.
    if ($this->currentEntity) {
      $this->removeEntityFromMemoryCache($this->currentEntity);
      $this->currentEntity = NULL;
    }

    // Empty entity set? Load the next chunk.
    if (empty($this->loadedEntities)) {
      // Get the next chunk, making sure we got one.
      $chunk = array_shift($this->idChunks);
      if (is_array($chunk)) {
        $this->loadedEntities = $this->entityStorage->loadMultiple($chunk);
      }
    }

    // Get the next entity from the loaded set. Can be FALSE here, which
    // is perfectly fine.
    $this->currentEntity = array_shift($this->loadedEntities);
  }

  /**
   * {@inheritdoc}
   */
  public function key(): mixed {
    if ($this->valid()) {
      // We use the entity id as a key.
      return $this->currentEntity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function valid(): bool {
    return !empty($this->currentEntity);
  }

  /**
   * {@inheritdoc}
   */
  public function rewind(): void {
    // Clean up first.
    $this->cleanUp();

    // Setup the chunks.
    $this->idChunks = array_chunk($this->ids, $this->chunkSize, TRUE);

    // Re-set the current variables.
    $this->currentEntity = NULL;
    $this->loadedEntities = [];

    // Initialise / get the first element.
    $this->next();
  }

  /**
   * Function to remove an entity from the memory cache.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to remove from the memory cache.
   */
  protected function removeEntityFromMemoryCache(EntityInterface $entity): void {
    $cid = 'values:' . $entity->getEntityTypeId() . ':' . $entity->id();
    $this->memoryCache->delete($cid);
  }

  /**
   * Function to check if an offset exists.
   *
   * @param mixed $offset
   *   The offset (entity id) to check that exists.
   *
   * @return bool
   *   Whether the offset exists, TRUE or FALSE.
   */
  public function offsetExists($offset): bool {
    return in_array($offset, $this->ids);
  }

  /**
   * Function to get the specific entry.
   *
   * @param mixed $offset
   *   The offset (entity id) to get.
   *
   * @return mixed|null
   *   The entity with the given id or NULL if it doesn't exist.
   */
  public function offsetGet($offset): mixed {
    if (!$this->offsetExists($offset)) {
      return NULL;
    }

    // It may or may not be loaded already, rather than messing about check
    // that, just load it and let Drupal's cache do it's thing.
    return $this->entityStorage->load($offset);
  }

  /**
   * Dummy implementation of offsetSet() which does nothing.
   *
   * @param mixed $offset
   *   The offset to set.
   * @param mixed $value
   *   The value to set - ignored.
   */
  public function offsetSet($offset, $value): void {
    // Intentionally left blank. Not relevant to us.
  }

  /**
   * Remove an entity id from the list of ids.
   *
   * @param mixed $offset
   *   The offset (entity id) to remove.
   */
  public function offsetUnset($offset): void {
    // First check if the current entity is the one we're unsetting. We need
    // to move on if that's the case.
    if ($this->currentEntity->id() == $offset) {
      $this->next();
    }

    // Find it in the master list of ids, if it's not there, we're done.
    $index = array_search($offset, $this->ids);
    if ($index === FALSE) {
      return;
    }

    // We definitely need to remove it from the master list of ids.
    unset($this->ids[$index]);

    // This means it can either be in the loaded entities or the remaining
    // chunks - or neither if it's been iterated past. We'll check loaded
    // entities first as that's indexed by the entity id.
    if (array_key_exists($offset, $this->loadedEntities)) {
      unset($this->loadedEntities[$offset]);
      return;
    }

    // Well, it's not in the loaded entities, search the chunks now.
    foreach ($this->idChunks as $chunk_index => $chunk) {
      $chunk_id_index = array_search($offset, $chunk);

      // We found it in this chunk, remove it and stop checking the chunks.
      if ($chunk_id_index !== FALSE) {
        unset($this->idChunks[$chunk_index][$chunk_id_index]);
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->ids);
  }

}
